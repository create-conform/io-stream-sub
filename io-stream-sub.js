/////////////////////////////////////////////////////////////////////////////////////////////
//
// io-stream-sub
//
//    IO Sub Stream Class.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var Error =     require("error");
var event =     require("event");
var stream =    require("io-stream");

/////////////////////////////////////////////////////////////////////////////////////////////
//
// StreamSub Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function StreamSub(stream, offset, size, name) {
    var own = this;
    var closed = false;
    var events = new event.Emitter(this);

    this.getName = function () {
        return name;
    };
    this.getLength = function () {
        return new Promise(function (resolve, refuse) {
            closed ? refuse(new Error(stream.ERROR_STREAM_CLOSED, "")) : resolve(size);
        });
    };
    this.read = function (len, position) {
        if (!position) {
            position = 0;
        }

        return new Promise(function (resolve, refuse) {
            if (closed) {
                refuse(new Error(stream.ERROR_STREAM_CLOSED, ""));
                return;
            }

            if (len == null) {
                len = size;
            }

            if (offset + position + len > offset + size) {
                len = (offset + size) - (offset + position);
            }
            return stream.read(len, offset + position).then(resolve).catch(refuse);
        });
    };
    this.write = function (data, position) {
        if (!position) {
            position = 0;
        }

        return new Promise(function (resolve, refuse) {
            if (closed) {
                refuse(new Error(stream.ERROR_STREAM_CLOSED, ""));
                return;
            }

            return stream.write(data, offset + position).then(resolve).catch(refuse);
        });
    };
    this.close = function () {
        return new Promise(function (resolve, refuse) {
            if (closed) {
                refuse(new Error(stream.ERROR_STREAM_CLOSED, ""));
                return;
            }

            closed = true;
            resolve();
        });
    };
};
StreamSub.prototype = stream;

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = StreamSub;